package com.example.maciej.xnforum;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.example.maciej.xnforum.adapters.PostAdapter;
import com.example.maciej.xnforum.logic.ForumService;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class ReplyActivity extends AppCompatActivity {
    private EditText edit_description;
    private CheckBox button_check_in;
    private String location = null;
    private int id;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reply);


        Bundle b = getIntent().getExtras();
        id = (int) b.get("id");
        edit_description = (EditText) findViewById(R.id.editText2);
        button_check_in = (CheckBox) findViewById(R.id.button_check_in);



        button_check_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button_check_in.isChecked()) {
                    location = getAddress();
                    if(location == null) {
                        Toast.makeText(getApplicationContext(), getString(R.string.gps_error), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), getString(R.string.your_location) + " " + location, Toast.LENGTH_SHORT).show();
                    }
                } else {
                    location = null;
                }
            }
        });

        ((Button) findViewById(R.id.button_reply)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tmp_desc = edit_description.getText().toString();
                if(location != null) {
                    tmp_desc += "\n - - - - - - - - - \n" + getString(R.string.sent_from)+ " " + location;
                }

                final String description = tmp_desc;

                new Thread() {
                    @Override
                    public void run() {
                        ForumService service = new ForumService();
                        final String result = service.addPost(id, description);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if(result.equals("ok") || result.equals("\"ok\"")){
                                    setResult(RESULT_OK, null);
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }.start();
            }
        });
    }


    private String getAddress(){
        GPSTracker gps = new GPSTracker(ReplyActivity.this);
        if(gps.canGetLocation()) { // gps enabled} // return boolean true/false
            System.out.println("ok");

            System.out.println(gps.getLatitude());
            System.out.println(gps.getLongitude());

            Geocoder gcd = new Geocoder(getApplicationContext(), Locale.getDefault());

            List<Address> addresses = null;
            try {
                addresses = gcd.getFromLocation(gps.getLatitude(), gps.getLongitude(), 1);

            } catch (IOException e) {
                return null;
            }
            if (addresses.size() > 0)
            {
                System.out.println(addresses.get(0).getLocality());
                return addresses.get(0).getLocality();
            }

        }

        return null;
    }


}
