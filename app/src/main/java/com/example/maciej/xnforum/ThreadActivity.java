package com.example.maciej.xnforum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.maciej.xnforum.adapters.PostAdapter;
import com.example.maciej.xnforum.logic.ForumService;
import com.example.maciej.xnforum.models.PostDisplayDto;
import com.example.maciej.xnforum.models.ThreadDisplayDto;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ThreadActivity extends GlobalActivity {
    private ArrayList<PostDisplayDto> listPosts;
    private ListView postsListView;
    PostAdapter postAdapter;
    private int page = 1;
    private int id;

    TextView threadNameTextView;
    TextView loginTextView;
    TextView descriptionTextView;
    TextView dateTextView;
    ImageView avatarImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);

        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
            .cacheInMemory(true)
                .cacheOnDisk(true)
            .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions).build();

        ImageLoader.getInstance().init(config);


        Bundle b = getIntent().getExtras();
        id = (int) b.get("id");

        postsListView = (ListView) findViewById(R.id.posts_list_view);

        threadNameTextView = (TextView)findViewById(R.id.thread_name);

        LinearLayout include = (LinearLayout)findViewById(R.id.op_post);

        loginTextView =
                (TextView) include.findViewById(R.id.user_login);
        descriptionTextView =
                (TextView) include.findViewById(R.id.description);
        dateTextView =
                (TextView) include.findViewById(R.id.date);
        avatarImageView =
                (ImageView) include.findViewById(R.id.user_avatar);

        Button button_reply = (Button)findViewById(R.id.button_reply);

        button_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!ForumService.isLogged()) {
                    Toast.makeText(getApplicationContext(), getString(R.string.not_logged), Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(ThreadActivity.this, ReplyActivity.class);
                intent.putExtra("id", id);

                startActivityForResult(intent, 1);
            }
        });

        loadDataAsync();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK){
            loadDataAsync();
        }
    }

    private void loadDataAsync(){
        final ForumService service = new ForumService();
        new Thread() {
            @Override
            public void run() {
                final ThreadDisplayDto model = service.GetThread(id, page);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listPosts = model.getPosts();

                        postAdapter = new PostAdapter(ThreadActivity.this, listPosts);
                        postsListView.setAdapter(postAdapter);

                        threadNameTextView.setText(model.getName());
                        loginTextView.setText(model.getPost().getUserLogin());
                        descriptionTextView.setText(model.getPost().getText());
                        DateFormat df = new SimpleDateFormat("HH:mm:ss dd-MM");
                        String stringDate = df.format(model.getPost().getDate());
                        dateTextView.setText(stringDate);
                        ImageLoader.getInstance().displayImage(model.getPost().getUserAvatar(), avatarImageView);

                    }
                });
            }
        }.start();
    }
}
