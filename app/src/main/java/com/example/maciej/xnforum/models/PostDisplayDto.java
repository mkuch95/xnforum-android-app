package com.example.maciej.xnforum.models;

import android.support.annotation.Nullable;

import java.util.Date;

/**
 * Created by Maciej on 2017-01-10.
 */

public class PostDisplayDto {
    public PostDisplayDto()
    {
    }

    private int id;
    @Nullable
    private int userId;

    private String userLogin;
    private String userAvatar;
    private Date date;
    private String text;
    //public PostStatusEnum status { get; set; }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Nullable
    public int getUserId() {
        return userId;
    }

    public void setUserId(@Nullable int userId) {
        this.userId = userId;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
