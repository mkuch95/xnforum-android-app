package com.example.maciej.xnforum.models;

import java.util.ArrayList;
import java.util.List;

public class AllForumDataDto {
    private ArrayList<AdminNoteDto> adminNotes = new ArrayList<>();
    private ArrayList<CategoryDto> categories = new ArrayList<>();

    public ArrayList<AdminNoteDto> getAdminNotes() {
        return adminNotes;
    }

    public void setAdminNotes(ArrayList<AdminNoteDto> adminNotes) {
        this.adminNotes = adminNotes;
    }

    public ArrayList<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<CategoryDto> categories) {
        this.categories = categories;
    }
}


