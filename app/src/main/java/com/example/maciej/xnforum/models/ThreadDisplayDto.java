package com.example.maciej.xnforum.models;

import java.util.ArrayList;

/**
 * Created by Maciej on 2017-01-10.
 */

public class ThreadDisplayDto {

    public ThreadDisplayDto()
    {
        posts = new ArrayList<PostDisplayDto>();
    }

    private int id;
    private String name;
    private Boolean forumAnonymousPosts;

    private PostDisplayDto post;

    private int forumId;
    private String forumName;

    private int categoryId;
    private String categoryName;

    private ArrayList<PostDisplayDto> posts;

    private int page;
    private int pagesCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getForumAnonymousPosts() {
        return forumAnonymousPosts;
    }

    public void setForumAnonymousPosts(Boolean forumAnonymousPosts) {
        this.forumAnonymousPosts = forumAnonymousPosts;
    }

    public PostDisplayDto getPost() {
        return post;
    }

    public void setPost(PostDisplayDto post) {
        this.post = post;
    }

    public int getForumId() {
        return forumId;
    }

    public void setForumId(int forumId) {
        this.forumId = forumId;
    }

    public String getForumName() {
        return forumName;
    }

    public void setForumName(String forumName) {
        this.forumName = forumName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public ArrayList<PostDisplayDto> getPosts() {
        return posts;
    }

    public void setPosts(ArrayList<PostDisplayDto> posts) {
        this.posts = posts;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }
}
