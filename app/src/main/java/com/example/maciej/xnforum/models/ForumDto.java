package com.example.maciej.xnforum.models;

import java.util.ArrayList;

/**
 * Created by Maciej on 2017-01-05.
 */

public class ForumDto {
    private int id;
    private String name;
    private int categoryId;
    private String categoryName;
    private int page;
    private int pagesCount;
    private ArrayList<ThreadItemDto> threads = new ArrayList<>();


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPagesCount() {
        return pagesCount;
    }

    public void setPagesCount(int pagesCount) {
        this.pagesCount = pagesCount;
    }

    public ArrayList<ThreadItemDto> getThreads() {
        return threads;
    }

    public void setThreads(ArrayList<ThreadItemDto> threads) {
        this.threads = threads;
    }
}
