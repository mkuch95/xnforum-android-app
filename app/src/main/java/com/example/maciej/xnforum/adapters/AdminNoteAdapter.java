package com.example.maciej.xnforum.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.maciej.xnforum.R;
import com.example.maciej.xnforum.models.AdminNoteDto;

import java.util.ArrayList;

/**
 * Created by Maciej on 2017-01-02.
 */

public class AdminNoteAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<AdminNoteDto> mDataSource;

    public AdminNoteAdapter(Context context, ArrayList<AdminNoteDto> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.list_item_note, parent, false);

        TextView titleTextView =
                (TextView) rowView.findViewById(R.id.note_text);

        AdminNoteDto note = (AdminNoteDto) getItem(position);

        titleTextView.setText(note.getText());

        return rowView;
    }

}
