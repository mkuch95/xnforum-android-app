package com.example.maciej.xnforum.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.example.maciej.xnforum.R;
import com.example.maciej.xnforum.models.CategoryDto;
import com.example.maciej.xnforum.models.ForumItemDto;

import java.util.ArrayList;

/**
 * Created by Maciej on 2017-01-02.
 */

public class CategoryAdapter extends BaseExpandableListAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<CategoryDto> mDataSource;

    public CategoryAdapter(Context context, ArrayList<CategoryDto> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getGroupCount() {
        return mDataSource.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mDataSource.get(groupPosition).getForums().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mDataSource.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mDataSource.get(groupPosition).getForums().get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return groupPosition*100 + childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        CategoryDto category = (CategoryDto) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group_category, null);
        }

        TextView categoryName = (TextView) convertView
                .findViewById(R.id.category_name);
        categoryName.setTypeface(null, Typeface.BOLD);//fixme delete
        categoryName.setText(category.getName());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final ForumItemDto childText = (ForumItemDto) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item_forum, null);
        }

        TextView forumName = (TextView) convertView
                .findViewById(R.id.forum_name);

        forumName.setText(childText.getName());

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
