package com.example.maciej.xnforum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.maciej.xnforum.logic.ForumService;
import com.example.maciej.xnforum.adapters.ThreadAdapter;
import com.example.maciej.xnforum.models.ForumDto;
import com.example.maciej.xnforum.models.ThreadItemDto;

import java.util.ArrayList;

public class ForumActivity extends GlobalActivity {
    private ArrayList<ThreadItemDto> listThreads;
    private ListView threadsListView;
    private ThreadAdapter threadsAdapter;
    private int page = 1;
    private int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum);

        Bundle b = getIntent().getExtras();
        id = (int) b.get("id");

        threadsListView = (ListView) findViewById(R.id.threads_list_view);

        threadsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ThreadItemDto thread = (ThreadItemDto) threadsAdapter.getItem(position);

                Intent intent = new Intent(ForumActivity.this, ThreadActivity.class);

                intent.putExtra("id", thread.getId());

                startActivity(intent);
            }
        });

        loadDataAsync();
    }

    private void loadDataAsync(){
        final ForumService service = new ForumService();
        new Thread() {
            @Override
            public void run() {
                final ForumDto model = service.GetForum(id, page);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listThreads = model.getThreads();
                        ((TextView) findViewById(R.id.forum_name)).setText(model.getName());
                        threadsAdapter = new ThreadAdapter(ForumActivity.this, listThreads);
                        threadsListView.setAdapter(threadsAdapter);

                    }
                });
            }
        }.start();
    }
}
