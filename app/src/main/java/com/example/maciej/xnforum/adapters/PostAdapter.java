package com.example.maciej.xnforum.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.maciej.xnforum.R;
import com.example.maciej.xnforum.models.PostDisplayDto;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by Maciej on 2017-01-02.
 */

public class PostAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<PostDisplayDto> mDataSource;

    public PostAdapter(Context context, ArrayList<PostDisplayDto> items) {
        mContext = context;
        mDataSource = items;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mDataSource.size();
    }

    @Override
    public Object getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = mInflater.inflate(R.layout.list_item_post, parent, false);

        TextView loginTextView =
                (TextView) rowView.findViewById(R.id.user_login);

        TextView descriptionTextView =
                (TextView) rowView.findViewById(R.id.description);

        TextView dateTextView =
                (TextView) rowView.findViewById(R.id.date);

        ImageView avatarImageView =
                (ImageView) rowView.findViewById(R.id.user_avatar);

        PostDisplayDto post = (PostDisplayDto) getItem(position);

        loginTextView.setText(post.getUserLogin());
        descriptionTextView.setText(post.getText());

        DateFormat df = new SimpleDateFormat("HH:mm:ss dd-MM");
        String stringDate = df.format(post.getDate());
        dateTextView.setText(stringDate);
        ImageLoader.getInstance().displayImage(post.getUserAvatar(), avatarImageView);

        return rowView;
    }

}
