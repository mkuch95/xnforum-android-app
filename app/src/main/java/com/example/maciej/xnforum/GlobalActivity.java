package com.example.maciej.xnforum;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.example.maciej.xnforum.logic.ForumService;

/**
 * Created by Maciej on 2017-01-19.
 */

public class GlobalActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if(ForumService.isLogged()) {
            getMenuInflater().inflate(R.menu.main_logged, menu);
        } else {
            getMenuInflater().inflate(R.menu.main, menu);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.item1:
                Intent intent = new Intent(GlobalActivity.this, LoginActivity.class);
                startActivity(intent);
                return true;
            case R.id.item2:
                ForumService service = new ForumService();
                service.Logout();
                finish();
                startActivity(getIntent());
                return true;
            case R.id.item3:
                finish();
                startActivity(getIntent());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
