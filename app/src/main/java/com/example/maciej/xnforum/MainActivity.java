package com.example.maciej.xnforum;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.maciej.xnforum.adapters.PostAdapter;
import com.example.maciej.xnforum.logic.ForumService;
import com.example.maciej.xnforum.adapters.AdminNoteAdapter;
import com.example.maciej.xnforum.adapters.CategoryAdapter;
import com.example.maciej.xnforum.models.AdminNoteDto;
import com.example.maciej.xnforum.models.AllForumDataDto;
import com.example.maciej.xnforum.models.CategoryDto;
import com.example.maciej.xnforum.models.ForumItemDto;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class MainActivity extends GlobalActivity {
    private ArrayList<AdminNoteDto> listNotes;
    private ArrayList<CategoryDto> listCategories;
    private ListView noteListView;
    private ExpandableListView categoryExpListView;
    private AdminNoteAdapter noteAdapter;
    private CategoryAdapter categoryAdapter;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //notes
        noteListView = (ListView) findViewById(R.id.note_list_view);

        //categories
        categoryExpListView = (ExpandableListView) findViewById(R.id.category_exp_list_view);


        categoryExpListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                ForumItemDto forum = (ForumItemDto) categoryAdapter.getChild(groupPosition,childPosition);

                Intent intent = new Intent(MainActivity.this, ForumActivity.class);

                intent.putExtra("id", forum.getId());

                startActivity(intent);

                return true;
            }
        });

        loadDataAsync();
    }


    private void loadDataAsync(){
        final ForumService service = new ForumService();
        new Thread() {
            @Override
            public void run() {
                final AllForumDataDto model = service.GetAllForumData();

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        listNotes = model.getAdminNotes();

                        noteAdapter = new AdminNoteAdapter(MainActivity.this, listNotes);
                        noteListView.setAdapter(noteAdapter);

                        listCategories = model.getCategories();

                        categoryAdapter = new CategoryAdapter(MainActivity.this, listCategories);
                        categoryExpListView.setAdapter(categoryAdapter);
                        for (int i = 0; i < categoryAdapter.getGroupCount(); i++) {
                            categoryExpListView.expandGroup(i);
                        }
                    }
                });
            }
        }.start();
    }
}
