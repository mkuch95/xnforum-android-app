package com.example.maciej.xnforum.models;

import java.util.ArrayList;

public class CategoryDto {
    private int id;
    private String name;
    public ArrayList<ForumItemDto> forums = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<ForumItemDto> getForums() {
        return forums;
    }

    public void setForums(ArrayList<ForumItemDto> forums) {
        this.forums = forums;
    }
}
