package com.example.maciej.xnforum;

import android.os.HandlerThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.maciej.xnforum.logic.ForumService;

public class LoginActivity extends AppCompatActivity {
    private EditText edit_login;
    private EditText edit_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edit_login = (EditText)findViewById(R.id.edit_login);
        edit_password = (EditText)findViewById(R.id.edit_password);

        ((Button)findViewById(R.id.button_login)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Thread() {
                    @Override
                    public void run() {
                        ForumService forumService = new ForumService();
                        final Boolean login = forumService.Login(edit_login.getText().toString(), edit_password.getText().toString());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                if (login) {
                                    Toast.makeText(getApplicationContext(), getString(R.string.login_ok), Toast.LENGTH_SHORT).show();
                                    finish();
                                } else {
                                    Toast.makeText(getApplicationContext(), getString(R.string.login_error), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                }.start();
            }
        });
    }
}
