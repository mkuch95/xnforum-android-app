package com.example.maciej.xnforum.logic;

import com.example.maciej.xnforum.models.AllForumDataDto;
import com.example.maciej.xnforum.models.ForumDto;
import com.example.maciej.xnforum.models.ThreadDisplayDto;
import com.example.maciej.xnforum.models.UserSessionDataDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

/**
 * Created by Maciej on 2017-01-10.
 */

public class ForumService {
    private static boolean logged = false;
    private static UserSessionDataDto userSessionData = null;//autoryzacja tylko na podstawie tego jest za słaba...
    public static boolean isLogged() {
        return logged;
    }

    private Gson gson;
    private String url = "http://192.168.43.172:3000/";
    private boolean mock = false;

    public ForumService(){
        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create();
    }

    public AllForumDataDto GetAllForumData(){
        String json;
        if(mock) {
            json = "{\"adminNotes\":[{\"id\":1,\"text\":\"Uwaga! przecena kamizelek bomber\"},{\"id\":1,\"text\":\"Inny wazny komunikat\"}],\"categories\":[{\"forums\":[{\"id\":7,\"name\":\"Spławik\",\"categoryId\":4,\"categoryName\":\"Wędkarstwo\"},{\"id\":8,\"name\":\"Gruntówka \",\"categoryId\":4,\"categoryName\":\"Wędkarstwo\"},{\"id\":9,\"name\":\"Mucha\",\"categoryId\":4,\"categoryName\":\"Wędkarstwo\"},{\"id\":13,\"name\":\"Inne metody\",\"categoryId\":4,\"categoryName\":\"Wędkarstwo\"}],\"id\":4,\"name\":\"Wędkarstwo\"},{\"forums\":[{\"id\":4,\"name\":\"Ogólne Rozmowy \",\"categoryId\":3,\"categoryName\":\"Forum ogólne\"},{\"id\":5,\"name\":\"PZW\",\"categoryId\":3,\"categoryName\":\"Forum ogólne\"}],\"id\":3,\"name\":\"Forum ogólne\"},{\"forums\":[{\"id\":14,\"name\":\"forum1\",\"categoryId\":5,\"categoryName\":\"kategoria2\"},{\"id\":15,\"name\":\"forum2\",\"categoryId\":5,\"categoryName\":\"kategoria2\"}],\"id\":5,\"name\":\"kategoria2\"}]}";
        } else {
            json= get("api/forum");
        }

        AllForumDataDto model = gson.fromJson(json, AllForumDataDto.class);
        //model.getAdminNotes().get(1).setText(test);
        return model;
    }

    public ForumDto GetForum(int id, int page){
        String json;
        if(mock) {
            json = "{\"page\":1,\"pagesCount\":1,\"threads\":[{\"id\":11,\"name\":\"Jakie akcesoria wędkarskie kupiłeś/kupiłaś\",\"views\":0,\"userId\":0,\"userLogin\":null},{\"id\":12,\"name\":\"zarybianie stawu\",\"views\":0,\"userId\":0,\"userLogin\":null},{\"id\":13,\"name\":\"jaka to ryba?\",\"views\":0,\"userId\":0,\"userLogin\":null},{\"id\":1011,\"name\":\"test stronnicowania\",\"views\":0,\"userId\":0,\"userLogin\":null}],\"id\":4,\"name\":\"Ogólne Rozmowy \",\"categoryId\":3,\"categoryName\":\"Forum ogólne\"}";
        } else {
            json = get("api/forum/" + id);
        }

        return gson.fromJson(json, ForumDto.class);
    }

    public ThreadDisplayDto GetThread(int id, int page){
        String json;
        if(mock) {
            json = "{\"id\":11,\"name\":\"Jakie akcesoria wędkarskie kupiłeś/kupiłaś\",\"views\":0,\"forumAnonymousPosts\":false,\"post\":{\"id\":42,\"userId\":1020,\"userLogin\":\"adam_pzw\",\"userAvatar\":\"1020.jpg\",\"date\":\"2016-12-12T21:14:15\",\"text\":\"Witam ten temat jest powiązany z akcesoriami wędkarskimi co kupiliście.\\r\\n\\r\\nMyślę że to dobry pomysł ponieważ można zobaczyć co ludzie kupują np haczyki,spławiki itd.\",\"status\":0,\"files\":[]},\"forumId\":4,\"forumName\":\"Ogólne Rozmowy \",\"categoryId\":3,\"categoryName\":\"Forum ogólne\",\"posts\":[{\"id\":43,\"userId\":11,\"userLogin\":\"ewa\",\"userAvatar\":\"default.png\",\"date\":\"2016-12-12T21:16:11\",\"text\":\"Ja kupiłam ostatnio wędkę z serii TURBOWYŁAWIACZ2000 EDYTOWANY PRZEZ MODERATORA!!\",\"status\":1,\"files\":[]},{\"id\":44,\"userId\":1021,\"userLogin\":\"radzio_plotka\",\"userAvatar\":\"1021.jpg\",\"date\":\"2016-12-12T21:17:32\",\"text\":\"Ja zainwestowałem w łódkę, mogę dzięki niej podróżować w nieznane zakątki łowiska\",\"status\":2,\"files\":[]}],\"postCreate\":{\"userId\":null,\"threadId\":11,\"text\":null,\"files\":[],\"modelState\":null},\"page\":1,\"pagesCount\":1}";
        } else {
            json = get("api/thread/"+id);
        }

        ThreadDisplayDto result = gson.fromJson(json, ThreadDisplayDto.class);
        for(int i = 0; i< result.getPosts().size(); i++) {
            result.getPosts().get(i).setUserAvatar(
                    url + "Content/Avatars/" + result.getPosts().get(i).getUserAvatar());
        }

        result.getPost().setUserAvatar(url + "Content/Avatars/" + result.getPost().getUserAvatar());


        return result;
    }

    public String addPost(int thread, String description){
        if(!isLogged())
            return "not logged!";

        int id = userSessionData.getId();

        try {
            description = URLEncoder.encode(description, "utf-8");
        } catch (Exception ex){}

        return post("/api/Thread", "id=" + id + "&thread=" + thread + "&description=" + description);
    }

    private String get(String _url){
        String parsedString = "";

        try {

            URL url = new URL(this.url + _url);
            URLConnection conn = url.openConnection();

            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            InputStream is = httpConn.getInputStream();
            parsedString = convertStreamToString(is);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return parsedString;
    }

    private String post(String _url, String post){
        String parsedString = "";

        try {

            URL url = new URL(this.url + _url);
            URLConnection conn = url.openConnection();

            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("POST");
            httpConn.connect();

            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(httpConn.getOutputStream()));
            writer.write(post);
            writer.flush();
            writer.close();


            InputStream is = httpConn.getInputStream();
            parsedString = convertStreamToString(is);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return parsedString;
    }

    private String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }


    public Boolean Login(String login, String password) {
        String json;
        json = post("api/account/", "login="+login+"&password="+password);
        userSessionData = gson.fromJson(json, UserSessionDataDto.class);

        if(userSessionData == null)
            return logged = false;

        return logged = true;
    }

    public void Logout() {
        logged = false;
        userSessionData = null;
    }
}
